import { BrowserRouter } from "react-router-dom";
import { Router, Routes, Route, Link, Navigate } from "react-router-dom";
import KeepNote from "./pages/keepNote";
// import Login from "../src/components/Login/Login";
// import Register from "../src/components/Register/Register";
// import PrivateRoute from "./components/PrivateRoute/PrivateRoute";
import "./App.css";
import React from "react";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<KeepNote />} />
          <Route path="/:slug" element={<KeepNote />} />
          <Route path="*" element={<Navigate to="/:slug" replace />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
