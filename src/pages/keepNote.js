import React, { useEffect, useState, useCallback } from "react";
import { BrowserRouter as Router, useNavigate, useParams } from "react-router-dom";
import ReactQuillEditor from "../components/Editor/ReactQuillEditor";
import { lockclose, lockopen, close } from "../helper/constant";
import "../App.css";
import debounce from "lodash.debounce";
import { Modal } from "@mui/material";
import Box from "@mui/material/Box";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";

const KeepNote = () => {
  const navigate = useNavigate();
  let { slug } = useParams();
  const [data, setData] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [password, setPassword] = useState();
  const [passwordData, setPasswordData] = useState();
  const [isModalData, setIsModalData] = useState("one");
  const [open, setOpen] = React.useState(false);
  const [isLock, setIsLock] = useState(false);
  const handleOpen = () => {
    setOpen(true);
    setIsModalData("one");
    setPassword("");
  };
  const handleClose = () => setOpen(false);
  useEffect(() => {
    if(slug){
      fetchData();
    }
    else{
      navigate(`/${Math.random().toString(36).slice(2)}`);
    }
    
  }, []);
  const updateQuery = () => {
    if (data != undefined && data.length >= 0) {
      postText();
    }
  };
  const delayedQuery = useCallback(debounce(updateQuery, 3000), [data]);
  useEffect(() => {
    delayedQuery();
    return delayedQuery.cancel;
  }, [data, delayedQuery]);
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    height: 340,
    transform: "translate(-50%, -50%)",
    width: 340,
    bgcolor: "#f3f1f1",
    boxShadow: 24,
    p: 4,
    borderRadius: 5,
    outline: "none",
    boxShadow:
      "0px 0px 15px 0px rgb(0 0 0 / 20%), 0px 24px 38px 3px rgb(0 0 0 / 14%), 0px 9px 46px 8px rgb(0 0 0 / 12%)",
  };
  const fetchData = async () => {
    try {
      const config = {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(
        `${process.env.REACT_APP_BASE_URL}item/${slug}`,
        config
      );
      const json = await response.json();
      if (json.message == "user not found data" || json.isProtected == 0) {
        setIsLock(false);
        setOpen(false);
        setData(json.data);
      } else {
        setIsModalData("two");
        setOpen(true);
        setIsLock(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const postText = async () => {
    setIsLoading(true);
    try {
      const config = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          ids: slug,
          data: data,
          password: password,
          passwordData: passwordData,
        }),
      };
      const response = await fetch(
        `${process.env.REACT_APP_BASE_URL}item/${slug}`,
        config
      );
      const json = await response.json();
      if (json.result.isProtected != 0) {
        setIsLock(true);
      } else {
        setIsLock(false);
      }
      setIsLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  const userPassword = async () => {
    try {
      const config = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ ids: slug, passwordData: passwordData }),
      };
      const response = await fetch(
        `${process.env.REACT_APP_BASE_URL}checkpassword/${slug}`,
        config
      );
      const json = await response.json();
      if (json.status == 200) {
        if (json.result.isProtected != 0) {
          setIsLock(true);
          setData(json.result.data);
          setOpen(false);
        } else {
          setIsLock(false);
          setOpen(true);
          setIsLoading(false);
        }
      } else {
        setIsLock(false);
        setOpen(true);
        setIsLoading(false);
        setPasswordData("");
      }
    } catch (error) {
      console.log(error);
    }
  };
  const addPassword = async () => {
    try {
      const config = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ ids: slug, password: password }),
      };
      const response = await fetch(
        `${process.env.REACT_APP_BASE_URL}password/${slug}`,
        config
      );
      const json = await response.json();
      if (json.result.password.length > 0) {
        setIsLock(true);
      } else {
        setIsLock(false);
      }
    } catch (error) {
      console.log(error);
    }
  };
  const passwordCheck = (e) => {
    e.preventDefault();
    setOpen(false);
    userPassword();
  };
  const addPasswordData = (e) => {
    e.preventDefault();
    setOpen(false);
    addPassword();
  };
  return (
    <>
      <div className="container">
        <div className="main">
          <Modal
            BackdropProps={{
              style: {
                backgroundColor: "transparent",
              },
            }}
            style={{
              backgroundColor: "rgba(255, 255, 255, 0.6)",
              backdropFilter: "blur(6px)",
            }}
            open={open}
            closeAfterTransition
          >
            <Fade in={open}>
              <Box sx={style}>
                {isModalData == "one" ? (
                  <>
                  {isLock && isLock ?<>
                  <div style={{ textAlign: "end" }}>
                      <span onClick={() => handleClose()}>
                        {" "}
                        <img
                          alt=""
                          src={close}
                          style={{
                            height: 15,
                            width: 15,
                            cursor: "pointer",
                          }}
                        ></img>
                      </span>
                    </div>
                    </>:<></>}
                    
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column",
                        marginTop: 85,
                      }}
                    >
                      <div style={{ textAlign: "center", fontSize: 30 }}>
                      {isLock && isLock ?"Update" :"Add"} Password
                      </div>

                      <form onSubmit={(e) => addPasswordData(e)}>
                        <div style={{ marginTop: 25 }}>
                          <input
                            style={{
                              width: "100%",
                              borderRadius: "0px",
                              borderBottom: "1px solid #808080",
                              borderTop: "0px solid ",
                              borderRight: "0px solid ",
                              borderLeft: "0px solid ",
                              backgroundColor: "transparent",
                              padding: "0px 8px",
                              height: 30,
                              outline: "none",
                              fontSize: 18,
                            }}
                            type="text"
                            value={password}
                            placeholder="Enter your password"
                            onChange={(e) => {
                              setPassword(e.target.value);
                            }}
                          />

                          <div style={{ width: "100%", textAlign: "center" }}>
                            {" "}
                            <button
                              style={{
                                width: "90px",
                                height: 35,
                                marginTop: 30,
                                backgroundColor: "#2b90fc",
                                border: "1px solid #2b90fc",
                                borderRadius: 5,
                                cursor: "pointer",
                                color: "white",
                              }}
                              type="submit"
                              onClick={() => setOpen(false)}
                            >
                              submit
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </>
                ) : (
                  <>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column",
                        marginTop: 85,
                      }}
                    >
                      <div style={{ textAlign: "center", fontSize: 30 }}>
                        Enter password
                      </div>
                      <form onSubmit={(e) => passwordCheck(e)}>
                        <div style={{ marginTop: 25 }}></div>
                        <input
                          style={{
                            width: "100%",
                            borderRadius: "0px",
                            borderBottom: "1px solid #808080",
                            borderTop: "0px solid ",
                            borderRight: "0px solid ",
                            borderLeft: "0px solid ",
                            backgroundColor: "transparent",
                            padding: "0px 8px",
                            height: 30,
                            outline: "none",
                            fontSize: 18,
                          }}
                          type="text"
                          value={passwordData}
                          placeholder="Enter your password"
                          onChange={(e) => setPasswordData(e.target.value)}
                        />
                        <div style={{ width: "100%", textAlign: "center" }}>
                          <button
                            style={{
                              width: "90px",
                              height: 35,
                              marginTop: 30,
                              backgroundColor: "#2b90fc",
                              border: "1px solid #2b90fc",
                              borderRadius: 5,
                              cursor: "pointer",
                              color: "white",
                            }}
                            type="submit"
                          >
                            submit
                          </button>
                        </div>
                      </form>
                    </div>
                  </>
                )}
              </Box>
            </Fade>
          </Modal>
          <div className="loader">
            <div>
              <Button onClick={handleOpen}>
                {isLock == false ? (
                  <img
                    alt=""
                    src={lockopen}
                    style={{
                      height: 45,
                      width: 34,
                    }}
                  ></img>
                ) : (
                  <img
                    alt=""
                    src={lockclose}
                    style={{
                      height: 45,
                      width: 34,
                    }}
                  ></img>
                )}
              </Button>
            </div>
            <div>{isLoading == true ? "Syncing....." :"Synced"}</div>
          </div>
          <ReactQuillEditor
            data={data}
            setData={setData}
            setIsLoading={setIsLoading}
          />
        </div>
      </div>
    </>
  );
};

export default KeepNote;
