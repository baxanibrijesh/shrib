import React from 'react';
import { BrowserRouter as Route, Redirect  } from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import { Navigate } from "react-router-dom";
const PrivateRoute = ({ component: Component, ...rest }) => {
    const navigate = useNavigate();
   const data = localStorage.getItem('token');
   console.log(rest,"rest");
   console.log(rest,"rest");
   return (
      <Route {...rest} render={props => (
         data ? (<Component {...props} />) : <Navigate to="/login" replace={true} />
      )} />
   );
};

export default PrivateRoute;