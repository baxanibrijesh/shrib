import React, { useState } from "react";

import { useNavigate } from "react-router-dom";
const Login = () => {
  let navigate = useNavigate();
  const [email,setEmail]=useState("")
  const [password,setPassword]=useState("")
  const LoginApi = async () => {
    try {
      const config = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email:email, password:password}),
      };
      const response = await fetch(
        `http://localhost:8000/login`,
        config
      );
      const json = await response.json();
      if(json && json){
        localStorage.setItem('token',JSON.stringify(json));
      }
      console.log(json);
    } catch (error) {
      console.log(error);
    }
  };
  const handleLogin = (e) =>{
    e.preventDefault();
    LoginApi()
    setEmail("")
    setPassword("")
  }

  return (
    <div className="container-fluid">
      <div
        className="form-container"
        style={{
          display: "grid",
          placeItems: "center",
          height: "100vh",
          width: "100vw",
        }}
      >
        <form
          className="pa-24"
           onSubmit={(e)=>handleLogin(e)}
        >
          <div style={{ textAlign: "center", marginBottom: 20 }}>Login</div>
          <div className="form-group">
            <label>Email</label>
            <input
              type="email"
              className="form-control react-form-input"
              id="email"
              onChange={(e)=>setEmail(e.target.value)}
              value={email}
              placeholder="Email"
            />
            {/* <Error field="email" /> */}
          </div>

          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              className="form-control react-form-input"
              id="password"
              value={password}
              onChange={(e)=>setPassword(e.target.value)}
              placeholder="Password"
            />
            {/* <Error field="password" /> */}
          </div>

          <button type="submit" className="btn form-button">
            Login
          </button>
          <div   style={{ marginTop:"20px" }}>
            New to Register?
            <span
              style={{ cursor: "pointer"}}
              onClick={() => navigate(`/register`)}
            >
              {" "}
              Register
            </span>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
