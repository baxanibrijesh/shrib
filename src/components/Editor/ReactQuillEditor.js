import React from "react";
import ReactQuill from "react-quill";
import "quill-mention";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
import "react-quill/dist/quill.core.css";
import ReactQuillEditorWrapper from "./ReactQuillEditorWrapper.style";

const ReactQuillEditor = (props) => {


  const { data, setData,setIsLoading} = props;

  return (
    <>
      <div
        className="mt-0 "
        style={{ backgroundColor: "#f4f5f7", borderRadius: 8 }}
      >
        <ReactQuillEditorWrapper>
          <ReactQuill
           preserveWhitespace="true"
            autoFocus
            value={
              data || ""
            }
            onChange={(content, delta, source, editor) => {
              setIsLoading(true)
              if (content == "<p><br></p>") {
                setData("");
              } else {
                setData(content);
              }
            }}
            rows={"3"}
            theme={"bubble"}
            placeholder={"Enter your note here! "}
            modules={ReactQuillEditor.modules}
            formats={ReactQuillEditor.formats}
          ></ReactQuill>
        </ReactQuillEditorWrapper>
      </div>
    </>
  );
};

ReactQuillEditor.modules = {
 
  toolbar: {
    container: [
      [{ header: "1" }, { header: "2" }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [{ list: "ordered" }, { list: "bullet" }],
    ],
    
  },
};
ReactQuillEditor.formats = [
  "autofocus",
  "header",
  "font",
  "size",
  "bold",
  "italic",
  "underline",
  "strike",
  "blockquote",
  "list",
  "bullet",
  "indent",
  "link",
  "image",
  "video",
  "mention",
];

export default ReactQuillEditor;
