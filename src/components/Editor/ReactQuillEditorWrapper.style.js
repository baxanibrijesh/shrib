import styled from "styled-components";

const ReactQuillEditorWrapper = styled.div`
  .ql-editor.ql-blank::before {
    color: #808080 !important;
    font-size: 24px !important;
  }
  .quill {
    min-height: 95px !important;
  }
  .ql-editor {
    min-height: 700px !important;
  }
  .ql-toolbar.ql-snow {
    border: none !important;
  }
  .ql-container.ql-snow {
    border: none !important;
  }
  .ql-editor ql-blank {
    min-height: 95px !important;
    max-height: 800px !important;
  }
  .ql-preview:after {
    content: "Preview";
  }
  .ql-editor p,
  .ql-editor ol,
  .ql-editor ul,
  .ql-editor pre,
  .ql-editor blockquote,
  .ql-editor h1,
  .ql-editor h2,
  .ql-editor h3,
  .ql-editor h4,
  .ql-editor h5,
  .ql-editor h6 {
    font-size: 22px !important;
  }
`;

export default ReactQuillEditorWrapper;



// useEffect(() => {
//   setIsLoading(true);
//  if(data != undefined){
//   const delayDebounceFn = setTimeout(() => {
//     postText();
//   }, 3000);
//   return () => clearTimeout(delayDebounceFn);
//  }
// }, [data]);



















// import React, { useEffect, useState, useCallback } from "react";
// import {
//   BrowserRouter as Router,
//   useParams,
//   useLocation,
// } from "react-router-dom";
// import ReactQuillEditor from "../components/Editor/ReactQuillEditor";
// import { lockclose, lockopen } from "../helper/constant";
// import "../App.css";
// import Loader from "../components/Loader/Loader";
// import debounce from "lodash.debounce";
// import { Modal } from "@mui/material";
// import Box from "@mui/material/Box";
// import Fade from "@mui/material/Fade";
// import Button from "@mui/material/Button";

// const Cart = () => {
//   let { slug } = useParams();
//   const [data, setData] = useState("");
//   const [isLoading, setIsLoading] = useState(false);
//   const [password, setPassword] = useState();
//   const [passwordData, setPasswordData] = useState();
//   const [isModalData, setIsModalData] = useState("one");
//   const [open, setOpen] = React.useState(false);
//   const [resData, setResData] = useState("");
//   const [isLock, setIsLock] = useState(false);
//   const handleOpen = () => {
//     setOpen(true);
//     setIsModalData("one");
//     setPassword("");
//   };

//   useEffect(() => {
//     fetchData();
//   }, []);
//   const updateQuery = () => {
//     if (data != undefined && data.length > 0) {
//       postText();
//     }
//   };
//   const delayedQuery = useCallback(debounce(updateQuery, 3000), [
//     data,
//   ]);
//   useEffect(() => {
//     delayedQuery();
//     return delayedQuery.cancel;
//   }, [data, delayedQuery]);
//   const style = {
//     position: "absolute",
//     top: "50%",
//     left: "50%",
//     height: "400px",
//     transform: "translate(-50%, -50%)",
//     width: 400,
//     bgcolor: "#f3f1f1",
//     boxShadow: 24,
//     p: 4,
//     borderRadius: 5,
//     outline: "none",
//     boxShadow: "0px 0px 15px 0px rgb(0 0 0 / 20%), 0px 24px 38px 3px rgb(0 0 0 / 14%), 0px 9px 46px 8px rgb(0 0 0 / 12%)"
//   };
//   const fetchData = async () => {
//     try {
//       // let tk = JSON.parse(localStorage.getItem("token"));
//       const config = {
//         method: "GET",
//         headers: {
//           Accept: "application/json",
//           "Content-Type": "application/json",
//           // 'Authorization': 'Bearer ' + tk.token,
//           // 'Host': 'api.producthunt.com'
//         },
//       };

//       const response = await fetch(
//         `http://localhost:8000/item/${slug}`,
//         config
//       );
//       const json = await response.json();
//       if (json.message == "user not found data" || json.isProtected == 0) {
//         setIsLock(false);
//         setOpen(false);
//       } else {
//         setIsModalData("two");
//         setOpen(true);
//         setIsLock(true);
//       }
//     } catch (error) {
//       console.log(error);
//     }
//   };

//   const postText = async () => {
//     setIsLoading(true);
//     try {
//       // let tk = JSON.parse(localStorage.getItem("token"));
//       const config = {
//         method: "POST",
//         headers: {
//           Accept: "application/json",
//           "Content-Type": "application/json",
//           // 'Authorization': 'Bearer ' + tk.token,
//           // 'Host': 'api.producthunt.com'
//         },
//         body: JSON.stringify({ ids: slug, data: data, password: password,passwordData:passwordData }),
//       };
//       const response = await fetch(
//         `http://localhost:8000/item/${slug}`,
//         config
//       );
//       const json = await response.json();
//       console.log(json,"json");
//       if (json.result.isProtected != 0) {
//         setIsLock(true);
//       } else {
//         setIsLock(false);
//       }
//       setIsLoading(false);
//     } catch (error) {
//       console.log(error);
//     }
//   }; 

//   const userPassword = async () => {
//     setIsLoading(true);
//     try {
//       // let tk = JSON.parse(localStorage.getItem("token"));
//       const config = {
//         method: "POST",
//         headers: {
//           Accept: "application/json",
//           "Content-Type": "application/json",
//           // 'Authorization': 'Bearer ' + tk.token,
//           // 'Host': 'api.producthunt.com'
//         },
//         body: JSON.stringify({ ids: slug,passwordData:passwordData }),
//       };
//       const response = await fetch(
//         `http://localhost:8000/checkpassword/${slug}`,
//         config
//       );
//       const json = await response.json();
//       console.log(json,"llll");
//       if(json.status == 200){
//         if (json.result.isProtected != 0 ) {
//           setIsLock(true);
//           setData(json.result.data);
//           setOpen(false);
//         }
//         else {
//           setIsLock(false);
//           setOpen(true);
//           setIsLoading(false);
//         }
//       }
//       else{
//         setIsLock(false);
//         setOpen(true);
//         setIsLoading(false);
//         console.log(json.message);
//       }
//     } catch (error) {
//       console.log(error);
//     }
//   };
//   const addPassword = async () => {
//     setIsLoading(true);
//     try {
//       // let tk = JSON.parse(localStorage.getItem("token"));
//       const config = {
//         method: "POST",
//         headers: {
//           Accept: "application/json",
//           "Content-Type": "application/json",
//           // 'Authorization': 'Bearer ' + tk.token,
//           // 'Host': 'api.producthunt.com'
//         },
//         body: JSON.stringify({ ids: slug,password:password }),
//       };
//       const response = await fetch(
//         `http://localhost:8000/password/${slug}`,
//         config
//       );
//       const json = await response.json();
//       // console.log(json,"llll");
//       // if (json.result.isProtected != 0) {
//       //   setIsLock(true);
//       // } else {
//       //   setIsLock(false);
//       // }
//       // setIsLoading(false);
//     } catch (error) {
//       console.log(error);
//     }
//   };
//   const passwordCheck = (e) => {
//     e.preventDefault();
//     setOpen(false);
//     userPassword();
   
//   }; 
//    const addPasswordData = (e) => {
//     e.preventDefault();
//     setOpen(false);
//     addPassword()
    
//   };
//   return (
//     <>
//       <div className="container">
//         <div className="main">
//           <Modal
//             BackdropProps={{
//               style: {
//                 backgroundColor: "transparent",
//               },
//             }}
//             style={{
//               backgroundColor: "rgba(255, 255, 255, 0.6)",
//               backdropFilter: "blur(6px)",
//             }}
//             open={open}
//             closeAfterTransition
//           >
//             <Fade in={open}>
//               <Box sx={style}>
//                 {isModalData == "one" ? (
//                   <>
//                     <div
//                       style={{
//                         display: "flex",
//                         alignItems: "center",
//                         justifyContent: "center",
//                         flexDirection: "column",
//                         marginTop: 85,
//                       }}
//                     >
//                       <div style={{ textAlign: "center", fontSize: 30 }}>
//                         Add Password
//                       </div>
//                       <form onSubmit={(e) => addPasswordData(e)}>
//                       <div style={{ marginTop: 25 }}>
//                         <div style={{ fontSize: 18, marginBottom: 10 }}>
//                           <label>Password</label>
//                         </div>

//                         <input
//                           style={{
//                             width: "100%",
//                             border: "1px solid #808080",
//                             borderRadius: 5,
//                             height: 30,
//                             outline: "none",
//                             fontSize: 18,
//                           }}
//                           type="text"
//                           value={password}
//                           onChange={(e) => {
//                             setPassword(e.target.value);
//                           }}
//                         />

//                         <div style={{ width: "100%", textAlign: "center" }}>
//                           {" "}
//                           <button
//                             style={{
//                               width: "90px",
//                               height: 35,
//                               marginTop: 30,
//                               backgroundColor: "#2b90fc",
//                               border: "1px solid #2b90fc",
//                               borderRadius: 5,
//                               cursor: "pointer",
//                               color: "white",
//                             }}
//                             type="submit"
//                             onClick={() => setOpen(false)}
//                           >
//                             submit
//                           </button>
//                         </div>
//                       </div>
//                       </form>
//                     </div>
//                   </>
//                 ) : (
//                   <>
//                     <div
//                       style={{
//                         display: "flex",
//                         alignItems: "center",
//                         justifyContent: "center",
//                         flexDirection: "column",
//                         marginTop: 85,
//                       }}
//                     >
//                       <div style={{ textAlign: "center", fontSize: 30 }}>
//                         Enter password
//                       </div>
//                       <form onSubmit={(e) => passwordCheck(e)}>
//                         <div style={{ marginTop: 25 }}>
//                           <div style={{ fontSize: 18, marginBottom: 10 }}>
//                             <label>Password</label>
//                           </div>
//                         </div>
//                         <input
//                           style={{
//                             width: "100%",
//                             border: "1px solid #808080",
//                             borderRadius: 5,
//                             height: 30,
//                             outline: "none",
//                             fontSize: 18,
//                           }}
//                           type="text"
//                           value={passwordData}
//                           onChange={(e) => setPasswordData(e.target.value)}
//                         />
//                         <div style={{ width: "100%", textAlign: "center" }}>
//                           <button
//                             style={{
//                               width: "90px",
//                               height: 35,
//                               marginTop: 30,
//                               backgroundColor: "#2b90fc",
//                               border: "1px solid #2b90fc",
//                               borderRadius: 5,
//                               cursor: "pointer",
//                               color: "white",
//                             }}
//                             type="submit"
//                           >
//                             submit
//                           </button>
//                         </div>
//                       </form>
//                     </div>
//                   </>
//                   )} 
//               </Box>
//             </Fade>
//           </Modal>
//           <div className="loader">
//             <div>
//               <Button onClick={handleOpen}>
//                 {isLock == false ? (
//                   <img
//                     alt=""
//                     src={lockopen}
//                     style={{
//                       height: 45,
//                       width: 34,
//                     }}
//                   ></img>
//                 ) : (
//                   <img
//                     alt=""
//                     src={lockclose}
//                     style={{
//                       height: 45,
//                       width: 34,
//                     }}
//                   ></img>
//                 )}
//               </Button>
//             </div>
//             <div>
//               {isLoading == true
//                 ? // <Loader height="34px" width="30px" />
//                   "syncing.........."
//                 : // <img
//                   //   alt=""
//                   //   src={lockclose}
//                   //   style={{
//                   //     height: 45,
//                   //     width: 34,
//                   //   }}
//                   // ></img>
//                   "synced"}
//             </div>
//           </div>
//           <ReactQuillEditor
//             data={data}
//             setData={setData}
//             setIsLoading={setIsLoading}
//           />
//         </div>
//       </div>
//     </>
//   );
// };

// export default Cart;
