import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const Register = () => {
  let navigate = useNavigate();
  const [name,setName]=useState("")
  const [email,setEmail]=useState("")
  const [password,setPassword]=useState("")


  const handleRegister =  (e) =>{
    e.preventDefault();
     RegisterApi()
    setName("")
    setEmail("")
    setPassword("")
  }
  const RegisterApi = async () => {
    try {
      const config = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({name:name,email:email,password:password}),
      };
      const response = await fetch(
        `http://localhost:8000/register`,
        config
      );
      const json = await response.json();
      console.log(json);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="container-fluid">
      <div
        className="form-container"
        style={{
          display: "grid",
          placeItems: "center",
          height: "100vh",
          width: "100vw",
        }}
      >
        <form
          className="pa-24"
           onSubmit={(e)=>handleRegister(e)}
        >
          <div style={{ textAlign: "center", marginBottom: 20 }}>Register</div>
          <div className="form-group">
            <label>Name</label>
            <input
              type="name"
              className="form-control react-form-input"
              id="name"
              onChange={(e)=>setName(e.target.value)}
              value={name}
              placeholder="Name"
            />
            {/* <Error field="email" /> */}
          </div>
          <div className="form-group">
            <label>Email</label>
            <input
              type="email"
              className="form-control react-form-input"
              id="email"
              onChange={(e)=>setEmail(e.target.value)}
              value={email}
              placeholder="Email"
            />
            {/* <Error field="email" /> */}
          </div>

          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              className="form-control react-form-input"
              id="password"
              value={password}
              onChange={(e)=>setPassword(e.target.value)}
              placeholder="Password"
            />
            {/* <Error field="password" /> */}
          </div>

          <button type="submit" className="btn form-button">
            Register
          </button>
          <div   style={{ marginTop:"20px" }}>
            Already have an account?{" "}
            <span
              style={{ cursor: "pointer" }}
              onClick={() => navigate(`/login`)}
            >
              {" "}
              Login
            </span>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Register;
